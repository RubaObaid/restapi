package resttest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.json.simple.JSONObject;

import io.restassured.http.ContentType;
import rest.*;
import org.testng.annotations.Test;

/**
 * 
 * @author Ruba
 *
 */
public class Post extends BaseTest{

	/**
	 * test Post when add valid id && test status code (testCase 1+2)
	 */
	@Test
	void validPost_12() {
		
		/**
		 * take information from file
		 */
		
		String jsonUser = Util.getFile("json/user.json").toString();
		// JSONObject jsonget = Util.getFile("json/user.json");
		
		given().
			headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
			body(jsonUser).
		when().
		
		/**
		 * add the information to the API
		 */
		
			post(Url.url).
		then().
			statusCode(200).
		assertThat().
			body("status", equalTo("SUCCESS")).
			body("message", equalTo("New employee is added successfully."));
/*
 		given().
			accept("application/json").get("Url.url").
		then()	
			.body(equalTo(jsonget));
			  */
		/**
		 * delete the employee
		 */
		
		given().
			contentType(ContentType.JSON).
			delete(Url.url + "/1");
		
	}


	/**
	 * test Post when add exists id 
	 */
	@Test
	void existsId_3() {

		String json = Util.getFile("json/origin.json").toString();
		
		given().
			headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
			body(json).
		when().
			post(Url.url).
		then().
			statusCode(200).
		assertThat().
			body("status", equalTo("ERROR")).
			body("message", equalTo("Employee already exists."));
		
	}


	/**
	 * test Post when add empty id 
	 */
	@Test
	void emptyId_4() {
		
		String json = Util.getFile("json/empty.json").toString();
		
		given().
			headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
			body(json).
		when().
			post(Url.url).
		then().
			statusCode(200).
		assertThat().
			body("status", equalTo("ERROR")).
			body("message", equalTo("Invalid value for employee id."));
		
	}

}