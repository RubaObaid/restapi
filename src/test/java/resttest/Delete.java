package resttest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;
import rest.*;

/**
 * 
 * @author Ruba
 *
 */

public class Delete extends BaseTest{


	/**
	 * test Delete when add valid id && test status code (testCase 1+2)
	 */
	
	@Test
	void validDelete_12() {
		
		//add data to delete it
		String jsonUser = Util.getFile("json/user.json").toString();
		
		given().
			headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
			body(jsonUser).
		when().
			post(Url.url);
		
		
		//delete data
		given().
			headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
		when().
			delete(Url.url + "/1").
		then().
			statusCode(200).
		assertThat().
			body("status", equalTo("SUCCESS")).
			body("message", equalTo("Employee with id 1 is deleted successfully."));
		
	}


	/**
	 * test Delete when add not existing id (testCase 3)
	 */
	@Test
	void notExistingId_3() {		
		
		given().
			headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
		when().
			delete(Url.url + "/4").
		then().
			statusCode(200).
		assertThat().
			body("status", equalTo("ERROR")).
			body("message", equalTo("Employee deosn't exist."));
	}


	/**
	 * test Delete all employee (testCase 4)
	 */
	@Test
	void allDelete_4() {
		
		given().
			headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
		when().
			delete(Url.url).
		then().
			statusCode(200).
		assertThat().
			body("status", equalTo("SUCCESS")).
			body("message", equalTo("All employees are deleted successfully."));

		/**
		 * add the origin information to the API
		 */
		String jsonUser = Util.getFile("json/origin.json").toString();
		String jsonUser2 = Util.getFile("json/origin2.json").toString();

		given().
			headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
			body(jsonUser).
		when().
			post(Url.url);

		given().
			headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).	
			body(jsonUser2).
		when().
			post(Url.url);
	}

}