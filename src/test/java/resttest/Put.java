package resttest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import org.testng.annotations.Test;
import io.restassured.http.ContentType;
import rest.*;

/**
 * 
 * @author Ruba
 *
 */

public class Put extends BaseTest{

	/**
	 * test Put when add valid id && test status code (testCase 1+2)
	 */
	
	@Test
	void validPut_12() {
		
		/**
		 * test data for Put
		 */
		
		String newjson = Util.getFile("json/update.json").toString();

		given().
			headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
			body(newjson).
		when().
			put(Url.url + "/438745745094").
		then().
			statusCode(200).
		assertThat().
			body("status", equalTo("SUCCESS")).
			body("message", equalTo("Employee with id 438745745094 is updated successfully."));


		/**
		 * Retrieve the original data
		 */
		String js = Util.getFile("json/origin.json").toString();
			
		given().
			headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
			body(js).
		when().
			put(Url.url + "/438745745094");
	}


	/**
	 * test data for Put when the id is not existing
	 */
	@Test
	void notExistingId_3() {
		
		String newjson = Util.getFile("json/update.json").toString();
		
		given().
			headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
			body(newjson).
		when().
			put(Url.url + "/7").
		then().
			statusCode(200).
		assertThat().
			body("status", equalTo("ERROR")).
			body("message", equalTo("Employee deosn't exist."));
	}


	/**
	 * test data for Put when the id is empty
	 */
	@Test
	void emptyId_4() {
		
		String newjson = Util.getFile("json/update.json").toString();
		
		given().
			headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
			body(newjson).
		when().
			put(Url.url).
		then().
			statusCode(405);
	}

}